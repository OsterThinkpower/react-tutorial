import React from 'react'

function Home(props) {
  const company = 'Company'
  const manager = 'Manager'

  return <div>
    This is Home Page
    <div>Company: {company}</div>
    <div>
      Manager: <input name='manager' value={manager}></input>
    </div>
  </div>
}

export default Home
