import React from 'react'

import OrderList from '../components/OrderList'
import OrderForm from '../components/OrderForm'

const initOrderList = [...new Array(5)].map((v, idx) => ({
  qty: 5 * idx,
  price: 200 - 5 * idx,
  icy: idx % 2 === 0 ? true : false
}))

const initNewOrder = {
  qty: '',
  price: '',
  icy: false
}

export default class Order extends React.Component {
  state = {
    orderList: initOrderList,
    newOrder: initNewOrder
  }

  handleRemoveOrder = idx => {
    const { orderList } = this.state
    const newList = [...orderList]
    newList.splice(idx, 1)

    this.setState({
      orderList: newList
    })
  }

  handleNewOrderValue = event => {
    const {
      name,
      value,
      type,
      checked
    } = event.target

    this.setState({
      newOrder: {
        ...this.state.newOrder,
        [name]: type === 'checkbox' ? checked : value
      }
    })
  }

  handleAddNewOrder = () => {
    const { orderList, newOrder } = this.state
    this.setState({
      orderList: [...orderList, newOrder],
      newOrder: {...initNewOrder}
    })
  }

  render () {
    const { orderList, newOrder } = this.state

    return <div>
      <OrderList
        data={orderList}
        onRemove={this.handleRemoveOrder} />
      <OrderForm
        newOrder={newOrder}
        onValueChange={this.handleNewOrderValue}
        onSubmit={this.handleAddNewOrder} />
    </div>
  }
}