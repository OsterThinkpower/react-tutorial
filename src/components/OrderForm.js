import React from 'react'

export default props => {
  const {
    newOrder: {
      qty,
      price,
      icy
    },
    onValueChange,
    onSubmit
  } = props

  const handleSubmit = e => {
    e.preventDefault()
    onSubmit()
  }

  return <div>
    <form onSubmit={handleSubmit}>
      <label>Quantity: <input name='qty' type='text' value={qty} onChange={onValueChange} /></label>
      <label>Price: <input name='price' type='text' value={price} onChange={onValueChange} /></label>
      <label>Icy: <input name='icy' type='checkbox' checked={icy} onChange={onValueChange} /></label>
      <button>Add Order</button>
    </form>
  </div>
}
