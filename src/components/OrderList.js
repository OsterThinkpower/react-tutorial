import React from 'react'

export default (props) => {
  const { data = [], onRemove } = props

  const handleRemove = idx => {
    onRemove(idx)
  }

  return <div>
    <table style={{marginLeft: 'auto', marginRight: 'auto'}}>
      <thead>
        <tr>
          <td>Quantity</td>
          <td>Price</td>
          <td>Icy</td>
          <td>Action</td>
        </tr>
      </thead>
      <tbody>
        {data.map((order, idx) => (
          <tr key={idx}>
            <td>{order.qty}</td>
            <td>{order.price}</td>
            <td> {order.icy ? 'Yes' : 'No'}</td>
            <td><button onClick={() => handleRemove(idx)}>-</button></td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
}