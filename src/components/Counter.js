import React from 'react'

const DisplayCount = (props) => <div>{props.count}</div>

export default class Counter extends React.Component {
  state = {
    count: 0
  }

  componentWillMount () {
    setInterval(this.tick, 1000)
  }

  tick = () => {
    this.setState({
      count: this.state.count + 1
    })
  }

  render () {
    return <div>
      This is Counter Component
      <div>
        Count is <DisplayCount count={this.state.count} />
      </div>
    </div>
  }
}