import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';

import HomePage from './pages/Home'
import OrderPage from './pages/Order'

class App extends Component {
  state = {
    page: 'home'
  }

  handleChangePage = event => {
    this.setState({
      page: event.target.id
    })
  }

  render() {
    const { page } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <div className="Header-content">
            This is React Tutorial
          </div>
        </header>

        <div className="App-body">
          <div className="Nav">
            <a id='home' href='#' onClick={this.handleChangePage}>Home</a>
            <a id='order' href='#order' onClick={this.handleChangePage}>Order</a>
          </div>
          <div className="PageContent">
            {page === 'home' && <HomePage />}
            {page === 'order' && <OrderPage />}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
