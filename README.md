# Thinkpower React　教學

## 前置作業
* 安裝nodeJS [網址](https://nodejs.org/en/download/)

  若有舊版本要套用在其 他專案，可使用nvm來管理 ( [mac](http://icarus4.logdown.com/posts/175092-nodejs-installation-guide)、[window](https://oranwind.org/nvm-windows/) )

* ES6 [網址](http://es6-features.org/#Constants) - ****很重要！！**** 下列為需要學會的特性
  - Arrow function
  - Extended Parameter Handling
  - Template Literals
  - Enhanced Object Properties
  - Destructuring Assignment
  - Modules
  - Promise
ES6練習題 - [網址](https://codesandbox.io/s/8yp1n7r5k0)


## 建立專案
快速建立專案 [Create React App](https://github.com/facebook/create-react-app).


## 啟動專案
專案目錄下執行
### `npm start`

啟動開發模式
打開瀏覽器 [http://localhost:3000](http://localhost:3000)



## 必須要會的 React/redux
進入DBS 專案前，務必要會以下模組，專案採取的架構為 React / Redux / Saga

- [React 基本概念](https://react.docschina.org/docs/hello-world.html)
- [React Router](https://reacttraining.com/react-router/web/guides/quick-start)
- [Redux](https://redux.js.org/)
- [React-redux](https://chentsulin.github.io/redux/docs/basics/UsageWithReact.html)
- [Redux Form](https://redux-form.com/)
- [Redux Saga](https://github.com/redux-saga/redux-saga)
- Styled Component

> 有提供程式碼可以參考，建議使用 git clone 後，再使用切換 branch 的方式來參考程式碼範例。
> 此範例是以下列列舉的項目的順序逐一完成，都由上一個範例再延續下去。

> 因此每一步之前的 branch 都可以切換來自己做練習。

#### React 基本概念
  - JSX
  - element
  - component & props
  - state & life cycle
  - event handling
  - list & keys
  - React form
  可由此開始練習 [Bitbucket 程式碼](https://bitbucket.org/OsterThinkpower/react-tutorial/src/master/)


##### React Router
  路由切換頁面，前端即可將頁面歷史記錄，透過瀏覽器上一步、下一步來做返回、前進

  > 可參考完成程式碼 [Bitbucket 分支 react-router](https://bitbucket.org/OsterThinkpower/react-tutorial/src/react-router//)

##### Redux
  中文版官方文件 [網址](https://chentsulin.github.io/redux/index.html)
  > 第一支 redux/reducer [Bitbucket 分支 redux](https://bitbucket.org/OsterThinkpower/react-tutorial/src/redux/)

##### React-redux
  配合React使用，藉由react-redux的連結，讓React Component能即時反應redux state的變動
  > 可參考完成程式碼 [Bitbucket 分支 react-redux](https://bitbucket.org/OsterThinkpower/react-tutorial/src/react-redux/)
  > 使用多個 Reducers 範例 [Bitbucket 分支 react-redux-reducers](https://bitbucket.org/OsterThinkpower/react-tutorial/src/react-redux-reducers/)

* **Redux Form**
  配合 redux 而成的表單，該函式庫已整合好 redux 並提供不錯常用的功能。
  在validation的使用上面，有許多靈活的方式可以使用
  可由此開始練習 [Bitbucket 分支 redux-form](https://bitbucket.org/OsterThinkpower/react-tutorial/src/redux-form/)

  * **使用 redux-form 原生 form元件**
    可參考完成程式碼 [Bitbucket 分支 redux-form-done](https://bitbucket.org/OsterThinkpower/react-tutorial/src/redux-form-done/)

  * **客製化輸入元件。即時輸入-即時顯示validation錯誤訊息。**
    可參考完成程式碼 [Bitbucket 分支 redux-form-customize](https://bitbucket.org/OsterThinkpower/react-tutorial/src/redux-form-customize/)

  * **加入redux作用域**
    當件元已 compose redux form 後，如果想要再與 redux 連接，以取得其他 state 資訊的使用方式。
    可參考完成程式碼 [Bitbucket 分支 redux-form-connect](https://bitbucket.org/OsterThinkpower/react-tutorial/src/redux-form-connect/)

##### Redux Saga
  處理前端畫面業務邏輯，提供同步、異步的功能，讓更新state、呼叫API不會有其他問題。
  > 前置前定程式碼 [Bitbucket 分支 saga](https://bitbucket.org/OsterThinkpower/react-tutorial/src/saga/)

  > 開始呼叫APP [Bitbucket 分支 saga-call](https://bitbucket.org/OsterThinkpower/react-tutorial/src/saga-call/)




